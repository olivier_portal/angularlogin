export class UserModel {
  firstName: string;
  lastName: string;
  birthdate: Date;
  email: string;
  password?: string;
}
