import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    this._initForm();
  }

  login(): void {
    const {login, password} = this.loginForm.value;
    this.authService.login$(login, password)
      .subscribe();
  }

  logout(): void {
    this.authService.logout$()
      .subscribe();
  }

  cancel(): void {
    this.loginForm.reset();
  }

  private _initForm(): void {
    this.loginForm = this.fb.group({
      login: ['', [Validators.required, Validators.minLength(6)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

}
