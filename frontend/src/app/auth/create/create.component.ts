import {ChangeDetectorRef, Component, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {NzModalRef, NzModalService} from 'ng-zorro-antd';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  createForm: FormGroup;
  isLoading = false;
  message = '';
  // isVisibleMiddle = false;
  tplModal?: NzModalRef;
  tplModalButtonLoading = false;
  htmlModalVisible = false;
  disabled = false;
  errorIcon = false;
  isSubmitted = false;
  errorMessage = {
    required: 'Tu dois remplir ce champs ou te battre',
    firstName: 'Hey ! Met ton prénom et que des lettres pas de chiffres, on aime pas les maths ici !!',
    lastName: 'Hey ! Met ton nom et que des lettres pas de chiffres, on aime pas les maths ici !!',
    birthdate: 'C\'est pas une date ça tu dois te battre !!!',
    email: 'Le mail n\'est pas au bon format : \'xxxx@xxxx.xxx\', Tu dois te battre !!!',
    password: `On ne parle pas du Seb's Fight Club donc ton mot de passe\n
     doit comporter entre 8 et 15 caractères avec au moins\n
      1 minuscule, 1 majuscule, 1 chiffre et 1 caractère spécial`,
    confirm: 'Euh... Achète toi des doigts ! T\'es pas capable de taper 2 fois le même mot de passe ??? Tu dois te battre !!!',
  };

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private cdr: ChangeDetectorRef,
    private modal: NzModalService,
  ) {
  }

  ngOnInit(): void {
    this._initForm();
  }

  validateConfirmPassword(): void {
    setTimeout(() => this.createForm.controls.confirm.updateValueAndValidity());
  }

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return {error: true, required: true};
    } else if (control.value !== this.createForm.controls.password.value) {
      return {confirm: true, error: true};
    }
    return {};
  }

  firstNameValidator = (control: FormControl): { [s: string]: boolean } => {
    const regex = RegExp('[a-zA-z]');
    if (control.value === '') {
      return {error: true, required: true};
    } else if (!regex.test(control.value)) {
      return {error: true, firstName: true};
    }
    return {};
  }

  lastNameValidator = (control: FormControl): { [s: string]: boolean } => {
    const regex = RegExp(/^[a-zA-z]$/);
    const str = control.value;
    if (str === '') {
      return {error: true, required: true};
    } else if (!regex.test(control.value)) {
      return {error: true, lastName: true};
    }
    return {};
  }

  emailValidator = (control: FormControl): { [s: string]: boolean } => {
    const regex = RegExp(/^(([^<>()[]\.,;:s@]+(.[^<>()[]\.,;:s@]+)*)|(.+))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA-Z-0-9]+.)+[a-zA-Z]{2,}))$/);
    const str = control.value;
    if (str === '') {
      return {error: true, required: true};
    } else if (!regex.test(control.value)) {
      return {error: true, email: true};
    }
    return {};
  }

  passwordValidator = (control: FormControl): { [s: string]: boolean } => {
    const regex = RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/);
    const str = control.value;
    if (str === '') {
      return {error: true, required: true};
    } else if (!regex.test(control.value)) {
      return {error: true, password: true};
    }
    return {};
  }

  createTplModal(tplTitle: TemplateRef<{}>, tplContent: TemplateRef<{}>, tplFooter: TemplateRef<{}>): void {
    this.tplModal = this.modal.create({
      nzTitle: tplTitle,
      nzContent: tplContent,
      nzFooter: tplFooter,
      nzMaskClosable: false,
      nzClosable: false,
      nzComponentParams: {
        value: this.message,
      },
      nzOnOk: () => console.log('Click ok')
    });
  }

  destroyTplModal(): void {
    this.tplModalButtonLoading = true;
    setTimeout(() => {
      this.tplModalButtonLoading = false;
      this.tplModal!.destroy();
    }, 1000);
  }

  createAccount(): void {
    this.isLoading = true;
    this.isSubmitted = false;
    this.errorIcon = false;
    this.authService.createAccount$(this.createForm.value)
      .pipe(
        tap((x) => this.isLoading = false),
        tap((x) => this.isSubmitted = true),
        tap((x) => this.message = 'Utilisateur bien créé'),
        tap((x) => this.cdr.detectChanges()),
        catchError(err => {
          console.log('IS LOADING = FALSE');
          return of(err)
            .pipe(
              tap((x) => console.error(err)),
              tap((x) => this.isLoading = false),
              tap((x) => this.isSubmitted = true),
              tap((x) => this.message = err),
              tap((x) => this.errorIcon = true),
              tap((x) => this.cdr.detectChanges()),
            );
        }),
      )
      .subscribe();
  }

  private _initForm(): void {
    this.createForm = this.fb.group({
      firstName: ['', [this.firstNameValidator]],
      lastName: ['', [this.lastNameValidator]],
      birthdate: ['', [Validators.required]],
      email: ['', [this.emailValidator]],
      password: ['', [this.passwordValidator]],
      confirm: ['', [this.confirmValidator]],
    });
  }

}
