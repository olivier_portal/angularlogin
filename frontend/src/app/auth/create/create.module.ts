import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateRoutingModule } from './create-routing.module';
import { CreateComponent } from './create.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NzAlertModule, NzButtonModule, NzDatePickerModule, NzFormModule, NzInputModule, NzModalModule, NzSpinModule} from 'ng-zorro-antd';


@NgModule({
  declarations: [CreateComponent],
  imports: [
    CommonModule,
    CreateRoutingModule,
    ReactiveFormsModule,
    NzSpinModule,
    NzAlertModule,
    NzFormModule,
    NzDatePickerModule,
    NzModalModule,
    NzInputModule,
    NzButtonModule
  ]
})
export class CreateModule { }
