import {Injectable} from '@angular/core';
import {BehaviorSubject, from, Observable, of} from 'rxjs';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import UserCredential = firebase.auth.UserCredential;
import {AngularFirestore} from '@angular/fire/firestore';
import {UserModel} from '../models/user.model';
import {AngularFireFunctions} from '@angular/fire/functions';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user$ = new BehaviorSubject(null);

  constructor(
    private angularFireAuth: AngularFireAuth,
    private aff: AngularFireFunctions,
    private afs: AngularFirestore,
    private router: Router,
  ) {
  }

  setUser(user): void {
    if (user) {
      this.user$.next({...user, isConnected: true});
    } else {
      this.user$.next({isConnected: false, user: null});
    }
  }

  getUser$(): Observable<any> {
    return this.user$.asObservable();
  }

  login$(login, password): Observable<any> {
    return from(this.angularFireAuth.signInWithEmailAndPassword(login, password))
      .pipe(
        switchMap((x) => this.router.navigate(['/home'])),
        catchError(err => of(err)
          .pipe(
            tap((x) => alert(x)),
          )
        )
      );
  }

  logout$(): Observable<any> {
    return from(this.angularFireAuth.signOut())
      .pipe(
        switchMap((x) => this.router.navigate(['/auth/login'])),
        catchError(err => of(err)
          .pipe(
            tap((x) => alert(x)),
          ))
      );
  }

  createAccount$(user: UserModel): Observable<any> {
    const {email, firstName, lastName, birthdate, password} = user;
    return this.aff.httpsCallable('doCreateUser')({email, firstName, lastName, birthdate, password})
      .pipe(
        tap((x) => console.log(x)),
        switchMap((x) => this.router.navigate(['/auth'])),
      );
  }

}
