import {Component, OnInit} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {AuthService} from './services/auth.service';
import {of} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  constructor(
    private angularFireAuth: AngularFireAuth,
    private router: Router,
    private authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    this.angularFireAuth.authState
      .pipe(
        tap((user) => this.authService.setUser(user)),
        switchMap((x) => this.router.navigate(['/auth'])),
        catchError(err => of(err)
          .pipe(
            tap((x) => alert(x)),
          ))
      )
      .subscribe();
  }
}
