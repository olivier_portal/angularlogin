import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CorpRoutingModule } from './corp-routing.module';
import { CorpComponent } from './corp.component';


@NgModule({
  declarations: [CorpComponent],
  imports: [
    CommonModule,
    CorpRoutingModule
  ]
})
export class CorpModule { }
