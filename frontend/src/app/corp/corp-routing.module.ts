import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CorpComponent } from './corp.component';

const routes: Routes = [{ path: '', component: CorpComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CorpRoutingModule { }
