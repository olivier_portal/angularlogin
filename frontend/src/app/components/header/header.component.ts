import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private authService: AuthService,
  ) {
  }

  isAuth: boolean;

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged(
      (user) => {
        this.isAuth = !!user;
      });
  }

  onSignOut(): void {
    this.authService.logout$();
  }

}
