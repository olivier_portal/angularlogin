// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCK1IfTkvzkpRR0oaSr0f7rvizztmExxQg',
    authDomain: 'angularlogin-7a230.firebaseapp.com',
    databaseURL: 'https://angularlogin-7a230.firebaseio.com',
    projectId: 'angularlogin-7a230',
    storageBucket: 'angularlogin-7a230.appspot.com',
    messagingSenderId: '81051891735',
    appId: '1:81051891735:web:c4a66a01761b73cfdcc452',
    measurementId: 'G-CWY292W9BB',
  },
  rootUrl: 'http://localhost:5000',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
