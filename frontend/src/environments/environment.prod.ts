export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCK1IfTkvzkpRR0oaSr0f7rvizztmExxQg',
    authDomain: 'angularlogin-7a230.firebaseapp.com',
    databaseURL: 'https://angularlogin-7a230.firebaseio.com',
    projectId: 'angularlogin-7a230',
    storageBucket: 'angularlogin-7a230.appspot.com',
    messagingSenderId: '81051891735',
    appId: '1:81051891735:web:c4a66a01761b73cfdcc452',
    measurementId: 'G-CWY292W9BB',
  },
  rootUrl: 'https://angularlogin-7a230.web.app/',
};
