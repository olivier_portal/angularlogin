import * as functions from 'firebase-functions';
import {UserModel} from './user.model';
import * as admin from 'firebase-admin';
import {cert} from './cert/cert';
import UserRecord = admin.auth.UserRecord;
import {HttpsError} from 'firebase-functions/lib/providers/https';

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://angularlogin-7a230.firebaseio.com'
});

const db = admin.firestore();

function testFirstName(firstName: string) {
    return /^[a-zA-Z]+$/.test(firstName);
}

function testLastName(lastName: string) {
    return /^[a-zA-Z]+$/.test(lastName);
}

function testBirthDate(birthDate: Date) {
    const date = new Date(birthDate);
    return Object.prototype.toString.call(date) === '[object Date]';
}

function testEmail (email: string) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

function testPassword(password: any)
{
    return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/.test(password);
}


export const doCreateUser = functions
    .region('europe-west3')
    .runWith({
        timeoutSeconds: 10,
        memory: '256MB',
    })
    .https
    .onCall(async (user: UserModel, context) => {
        try {
            const {email, password, firstName, lastName, birthdate} = user;
            if (!email || !password || !firstName || !lastName) {
                console.log('TEST : ' + email, password, birthdate, lastName, firstName);
                throw new HttpsError('invalid-argument', 'Tu dois remplir tous les champs ou te battre');
            }
            if (!testFirstName(firstName)) {
                throw new HttpsError('invalid-argument', 'Hey ! Met ton prénom et que des lettres pas de chiffres, on aime pas les maths ici !!');
            }
            if (!testLastName(lastName)) {
                throw new HttpsError('invalid-argument', 'Hey ! Met ton nom et que des lettres pas de chiffres, on aime pas les maths ici !!');
            }
            if (!testBirthDate(birthdate)) {
                throw new HttpsError('invalid-argument', 'C\'est pas une date ça tu dois te battre !!!');
            }
            if (!testEmail(email)) {
                    throw new HttpsError('invalid-argument', `Le mail n'est pas au bon format : 'xxxx@xxxx.xxx', Tu dois te battre !!!`);
            }
            if (!testPassword(password)) {
                throw new HttpsError('invalid-argument', `On ne parle pas du Seb's Fight Club donc ton mot de passe doit comporter entre 8 et 15 caractères avec au moins
                1 minuscule, 1 majuscule, 1 chiffre et 1 caractère spécial`);
            }
            const userRecord: UserRecord = await admin.auth().createUser({
                email,
                emailVerified: false,
                password,
                displayName: firstName + ' ' + lastName,
                disabled: false,
            });
            console.log(userRecord);
            await db.collection('users').doc(userRecord.uid).set({email, firstName, lastName, birthdate});

            return {info: 'created', data: {...user, uid: userRecord.uid}};
        } catch (e) {
            throw e;
        }
    });
