export interface UserModel {
  firstName: string;
  lastName: string;
  birthdate: Date;
  email: string;
  password?: string;
}
